<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
include("database/connect.php");

$sql = "SELECT carrier,aircraft_type,schedule_date, COUNT(CAST (strftime('%w', schedule_date) AS Integer)) as num , CAST (strftime('%w', schedule_date) AS Integer) as day_num FROM Flight_Information WHERE flight_type = 'Arrival' GROUP BY carrier,aircraft_type,CAST (strftime('%w', schedule_date) AS Integer) ORDER BY schedule_date";
// 0 is sunday
$result = $db->query($sql);
$data_json = array();
while ($row = $result->fetchArray(SQLITE3_ASSOC)){
  $data_json[] = $row;
}
$sql = "SELECT carrier,aircraft_type,schedule_date, COUNT(CAST (strftime('%w', schedule_date) AS Integer)) as num , CAST (strftime('%w', schedule_date) AS Integer) as day_num FROM Flight_Information WHERE flight_type = 'Departure' GROUP BY carrier,aircraft_type,CAST (strftime('%w', schedule_date) AS Integer) ORDER BY schedule_date";
// 0 is sunday
$result = $db->query($sql);
$data_json2 = array();
while ($row = $result->fetchArray(SQLITE3_ASSOC)){
  $data_json2[] = $row;
} // departure
unset($db);

$data = array( 'arrival' => $data_json, 
                       'departure' => $data_json2 );

echo json_encode($data);
