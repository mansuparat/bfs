$(document).ready(function () {
    $.get('triangle1.txt', function(data){
            make_dimesion1(data)
    });
    $.get('triangle2.txt', function(data){
            make_dimesion2(data)
    });
});

function make_dimesion1(data)
{
    var search_array = [];
    search_array = data.split('\n');
            var array_triangle1 = [];
            var html = "";
            for(var i = 0; i < search_array.length;i++)
            {
                var split_array = search_array[i].split(/[ ,]+/)
                for(var m = 0; m < split_array.length; m++)
                {
                    split_array[m] = parseInt(split_array[m])
                }
                array_triangle1.push(split_array)
            }
            
            var array_keep_path = []
            var array_keep = []
            for(var i = array_triangle1.length; i !== 1; i--)
            {
                array_keep_path = [];
                var row = [];
                var current = array_triangle1[i-2]; // ตำแหน่ง array ตัวรองสุดท้าย ปัจจุบัน เช่น 15 - 2 = 13
                var currentLen = current.length; // จำนวนตัวแปร current เช่น array_triangle1[14] ในนั้นมีตัวเลขทั้งหมด 14 ตัว
                var end = array_triangle1[i-1]; // ตำแหน่ง array ตัวสุดท้าย เช่น 15 - 1 = 14
                for ( var m = 0; m < currentLen; m++ ) {
                row.push(Math.max(current[m] + end[m] || 0, current[m] + end[m+1] || 0) ) // หาค่ารวมมากสุด เช่น ตำแหน่งแรก array อยู่ที่ 0 เอาค่ามาก สุด array[13][0] +  array[14][0] หรือ array[13][0] +  array[14][1] ถ้ามากกว่า   
                var max = Math.max(current[m] + end[m] || 0, current[m] + end[m+1] || 0);
                if(current[m] + end[m+1] > current[m] + end[m])
                {
                    array_keep_path.push({current: current[m] , end : end[m+1], max_row : max, row_index: i-1,index_num : m})
                }
                else
                {
                    array_keep_path.push({current: current[m] , end : end[m], max_row : max, row_index: i-1,index_num : m})
                }
                }
                 array_keep.push(array_keep_path)
                 array_triangle1.pop(); // เอา array ตัวสุดท้ายออก เช่น 15 - 1 = 14
                 array_triangle1.pop(); // เอา array ตัวรองสุดท้ายออก เช่น 14 - 1 =13
                  //  console.log(row)
                 array_triangle1.push(row); // เอาค่า max push ไป เป็นตัวสุดท้าย
            }
            console.log(array_triangle1)
            
            array_keep = array_keep.reverse()
            console.log(array_keep)
            var array_result = []
            var array_result_total = []
            var tmp_end = 0
            var tmp_index_num = 0
            for(var i = 0; i < array_keep.length; i++)
            {
                if(i == 0)
                {
                    tmp_end = array_keep[i][0]['end'];
                    tmp_index_num = array_keep[i][0]['index_num'];
                }
              
                for(var m =0; m < array_keep[i].length;m++)
                {
//                    console.log(tmp_end)
                    if(i == 0)
                    {
                        array_result = array_keep[i][m]
                    }
                    else
                    {
                    if( array_keep[i][m]['max_row'] == tmp_end && (tmp_index_num >= array_keep[i][m]['index_num'] - 1 && tmp_index_num <= array_keep[i][m]['index_num'] + 1))
                    {
                        array_result = array_keep[i][m]
                        tmp_end = array_keep[i][m]['end']
                        tmp_index_num = array_keep[i][m]['index_num']
                        break;
                    }
                    }
                }
                array_result_total.push(array_result)
            }
//            array_result_total = array_result_total.reverse();
            console.log(array_result_total)
            html += '<div class="wrapper">'
           for(var i = 0; i < search_array.length;i++)
            {
                 var current = 0;
                var index_current = 0
                if(i < search_array.length-1)
                {
                current = array_result_total[i]['current']
                index_current = array_result_total[i]['index_num']
                } 
                else
                {
                current = array_result_total[i-1]['end']
                index_current = array_result_total[i-1]['index_num']
                }
                var split_array = search_array[i].split(/[ ,]+/)
                
                    html += '<div class="row2">'
                for(var m = 0; m < split_array.length; m++)
                {
                    
                    if(current == parseInt(split_array[m]) && m == index_current)
                    {
                        html += '<div class="block" style="background-color: #f56a6a">'+split_array[m]+'</div>'
                    }
                    else
                    {
                    html += '<div class="block">'+split_array[m]+'</div>'
                    }
                    
                }
                html += '</div>'
                
            }
            html += '</div>'
            $('#show_pic_dimesion1').append(html);
            var string_result_dimesion = "";
            for(var i = 0 ;i < array_result_total.length; i++)
            {
                if(i == 0)
                {
                    string_result_dimesion += ""+array_result_total[i]['current']+""
                } else
                {
                    string_result_dimesion += " + "+array_result_total[i]['current']+""
                }
                if(i == array_result_total.length - 1)
                {
                    string_result_dimesion += " + "+array_result_total[i]['end']+""
                    string_result_dimesion += " = "+array_triangle1[0]+""
                }
                
                
            }
            $('#result_dimesion1').text('The result sum is '+string_result_dimesion);
}

function make_dimesion2(data)
{
    var search_array = [];
    search_array = data.split('\n');
            var array_triangle1 = [];
            var html = "";
            for(var i = 0; i < search_array.length;i++)
            {
                var split_array = search_array[i].split(/[ ,]+/)
                for(var m = 0; m < split_array.length; m++)
                {
                    split_array[m] = parseInt(split_array[m])
                }
                array_triangle1.push(split_array)
            }
            
            var array_keep_path = []
            var array_keep = []
            for(var i = array_triangle1.length; i !== 1; i--)
            {
                array_keep_path = [];
                var row = [];
                var current = array_triangle1[i-2]; // ตำแหน่ง array ตัวรองสุดท้าย ปัจจุบัน เช่น 15 - 2 = 13
                var currentLen = current.length; // จำนวนตัวแปร current เช่น array_triangle1[14] ในนั้นมีตัวเลขทั้งหมด 14 ตัว
                var end = array_triangle1[i-1]; // ตำแหน่ง array ตัวสุดท้าย เช่น 15 - 1 = 14
                for ( var m = 0; m < currentLen; m++ ) {
                row.push(Math.max(current[m] + end[m] || 0, current[m] + end[m+1] || 0) ) // หาค่ารวมมากสุด เช่น ตำแหน่งแรก array อยู่ที่ 0 เอาค่ามาก สุด array[13][0] +  array[14][0] หรือ array[13][0] +  array[14][1] ถ้ามากกว่า   
                var max = Math.max(current[m] + end[m] || 0, current[m] + end[m+1] || 0);
                if(current[m] + end[m+1] > current[m] + end[m])
                {
                    array_keep_path.push({current: current[m] , end : end[m+1], max_row : max, row_index: i-1,index_num : m})
                }
                else
                {
                    array_keep_path.push({current: current[m] , end : end[m], max_row : max, row_index: i-1,index_num : m})
                }
                }
                 array_keep.push(array_keep_path)
                 array_triangle1.pop(); // เอา array ตัวสุดท้ายออก เช่น 15 - 1 = 14
                 array_triangle1.pop(); // เอา array ตัวรองสุดท้ายออก เช่น 14 - 1 =13
                  //  console.log(row)
                 array_triangle1.push(row); // เอาค่า max push ไป เป็นตัวสุดท้าย
            }
            console.log(array_triangle1)
            
            array_keep = array_keep.reverse()
            console.log(array_keep)
            var array_result = []
            var array_result_total = []
            var tmp_end = 0
            var tmp_index_num = 0
            for(var i = 0; i < array_keep.length; i++)
            {
                if(i == 0)
                {
                    tmp_end = array_keep[i][0]['end'];
                    tmp_index_num = array_keep[i][0]['index_num'];
                }
              
                for(var m =0; m < array_keep[i].length;m++)
                {
//                    console.log(tmp_end)
                    if(i == 0)
                    {
                        array_result = array_keep[i][m]
                    }
                    else
                    {
                        console.log(tmp_index_num)
                    if( array_keep[i][m]['max_row'] == tmp_end && (tmp_index_num >= array_keep[i][m]['index_num'] - 1 && tmp_index_num <= array_keep[i][m]['index_num'] + 1))
                    {
                        array_result = array_keep[i][m]
                        tmp_end = array_keep[i][m]['end']
                        tmp_index_num = array_keep[i][m]['index_num']
                        break;
                    }
                    }
                }
                array_result_total.push(array_result)
            }
//            array_result_total = array_result_total.reverse();
            console.log(array_result_total)
            html += '<div class="wrapper">'
           for(var i = 0; i < search_array.length;i++)
            {
                var current = 0;
                var index_current = 0
                if(i < search_array.length-1)
                {
                current = array_result_total[i]['current']
                index_current = array_result_total[i]['index_num']
                } 
                else
                {
                current = array_result_total[i-1]['end']
                index_current = array_result_total[i-1]['index_num']
                }
                var split_array = search_array[i].split(/[ ,]+/)
                
                    html += '<div class="row2">'
                for(var m = 0; m < split_array.length; m++)
                {
                    
                    if(current == parseInt(split_array[m]) && m == index_current)
                    {
                        html += '<div class="block" style="background-color: #f56a6a">'+split_array[m]+'</div>'
                    }
                    else
                    {
                    html += '<div class="block">'+split_array[m]+'</div>'
                    }
                    
                }
                html += '</div>'
                
            }
            html += '</div>'
            $('#show_pic_dimesion2').append(html);
            var string_result_dimesion = "";
            for(var i = 0 ;i < array_result_total.length; i++)
            {
                if(i == 0)
                {
                    string_result_dimesion += ""+array_result_total[i]['current']+""
                } else
                {
                    string_result_dimesion += " + "+array_result_total[i]['current']+""
                }
                if(i == array_result_total.length - 1)
                {
                    string_result_dimesion += " + "+array_result_total[i]['end']+""
                    string_result_dimesion += " = "+array_triangle1[0]+""
                }
                
                
            }
            $('#result_dimesion2').text('The result sum is '+string_result_dimesion);
}