$(document).ready(function () {
    BlockUI()
    get_carrier();
});

function get_carrier()
{
    $.ajax({
            type: 'GET',
            url: 'engine_flight_carrier.php',
            dataType: 'json',
            data: {
                
            },
            success: function (data) {
                var table_html_arrival = "";
                var table_html_departure = "";
                if(data['arrival'].length > 0)
                {
                    $('#table_carrier_arrival').empty();
                    table_html_arrival = make_carrier_table(data['arrival'])
                    $('#table_carrier_arrival').append(table_html_arrival);
                }
                else
                {
                    $( "#div_carrier_arrival" ).remove();
                }
                
                if(data['departure'].length > 0)
                {
                    $('#table_carrier_departure').empty();
                    table_html_departure = make_carrier_table(data['departure'])
                    $('#table_carrier_departure').append(table_html_departure);
                }
                else
                {
                    $( "#div_carrier_departure" ).remove();
                }
                
                get_aircraft();
                

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest);
            }
        });
}
function make_carrier_table(data)
{
    var group_to_values = data.reduce(function (obj, item) {
                obj[item.carrier] = obj[item.carrier] || [];
                obj[item.carrier].push({num:item.num,day_num:item.day_num});
                return obj;
                }, {});
                
                var groups = Object.keys(group_to_values).map(function (key) {
                    var sunday = 0;
                    var monday = 0;
                    var tuesday = 0;
                    var wednesday = 0;
                    var thursday = 0;
                    var thursday = 0
                    var friday = 0;
                    var saturday = 0;
                    for(var i =0; i < group_to_values[key].length; i++)
                    {
                        switch(group_to_values[key][i]['day_num']) {
                        case 0:
                          sunday = group_to_values[key][i]['num']
                          break;
                        case 1:
                          monday = group_to_values[key][i]['num']
                          break;
                        case 2:
                          tuesday = group_to_values[key][i]['num']
                          break;
                        case 3:
                          wednesday = group_to_values[key][i]['num']
                          break;
                        case 4:
                          thursday = group_to_values[key][i]['num']
                          break;
                        case 5:
                          friday = group_to_values[key][i]['num']
                          break;
                       case 6:
                          saturday = group_to_values[key][i]['num']
                          break;
                        default:
                          console.log('something wrong')
                      }
                  }
                  return {carrier_name: key , sunday: sunday , monday : monday , tuesday : tuesday , wednesday : wednesday , thursday : thursday , friday : friday , saturday : saturday};

                });
                
                var table_html = "";
                table_html += "<table class='d_table display responsive'>";
                table_html += "<thead>";
                table_html += "<tr>";
                table_html += "<th>Total Flights</th>";
		table_html += "<th colspan='7' style='text-align: center'>Weekday</th>"
                table_html += "</tr>";
                table_html += "<tr>";
                table_html += "<th>Carrier</th>";
		table_html += "<th>Mon</th>";
		table_html += "<th>Tue</th>";
                table_html += "<th>Wed</th>";
                table_html += "<th>Thu</th>";
                table_html += "<th>Fri</th>";
                table_html += "<th>Sat</th>";
                table_html += "<th>Sun</th>";
                table_html += "<th></th>";
                table_html += "</tr>";
                table_html += "</thead>";
                table_html += "<tbody>";
                for(var i = 0; i < groups.length; i++)
                {
                    table_html += "<tr>";
                    table_html += "<td>"+groups[i]['carrier_name']+"</td>";
                    table_html += "<td>"+groups[i]['monday']+"</td>";
                    table_html += "<td>"+groups[i]['tuesday']+"</td>";
                    table_html += "<td>"+groups[i]['wednesday']+"</td>";
                    table_html += "<td>"+groups[i]['thursday']+"</td>";
                    table_html += "<td>"+groups[i]['friday']+"</td>";
                    table_html += "<td>"+groups[i]['saturday']+"</td>";
                    table_html += "<td>"+groups[i]['sunday']+"</td>";
                    table_html += "<td></td>";
                    table_html += "</tr>";
                }
                table_html += "</tbody>";
                table_html += "</table>";
                return table_html;
}


function get_aircraft()
{
    $.ajax({
            type: 'GET',
            url: 'engine_flight_aircraft.php',
            dataType: 'json',
            data: {
                
            },
            success: function (data) {
                
                var table_html_arrival = "";
                var table_html_departure = "";
                if(data['arrival'].length > 0)
                {
                    $('#table_aircraft_arrival').empty();
                    table_html_arrival = make_aircraft_table(data['arrival'])
                    $('#table_aircraft_arrival').append(table_html_arrival);
                }
                else
                {
                    $( "#div_aircraft_arrival" ).remove();
                }
                
                if(data['departure'].length > 0)
                {
                    $('#table_aircraft_departure').empty();
                    table_html_departure = make_aircraft_table(data['departure'])
                    $('#table_aircraft_departure').append(table_html_departure);
                }
                else
                {
                    $( "#div_aircraft_departure" ).remove();
                }
                
                get_carrier_aircraft();
                

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest);
            }
        });
}

function make_aircraft_table(data)
{
    var group_to_values = data.reduce(function (obj, item) {
                obj[item.aircraft_type] = obj[item.aircraft_type] || [];
                obj[item.aircraft_type].push({num:item.num,day_num:item.day_num});
                return obj;
                }, {});
                
                var groups = Object.keys(group_to_values).map(function (key) {
                    var sunday = 0;
                    var monday = 0;
                    var tuesday = 0;
                    var wednesday = 0;
                    var thursday = 0;
                    var thursday = 0
                    var friday = 0;
                    var saturday = 0;
                    for(var i =0; i < group_to_values[key].length; i++)
                    {
                        switch(group_to_values[key][i]['day_num']) {
                        case 0:
                          sunday = group_to_values[key][i]['num']
                          break;
                        case 1:
                          monday = group_to_values[key][i]['num']
                          break;
                        case 2:
                          tuesday = group_to_values[key][i]['num']
                          break;
                        case 3:
                          wednesday = group_to_values[key][i]['num']
                          break;
                        case 4:
                          thursday = group_to_values[key][i]['num']
                          break;
                        case 5:
                          friday = group_to_values[key][i]['num']
                          break;
                       case 6:
                          saturday = group_to_values[key][i]['num']
                          break;
                        default:
                          console.log('something wrong')
                      }
                  }
                  return {aircraft_type: key , sunday: sunday , monday : monday , tuesday : tuesday , wednesday : wednesday , thursday : thursday , friday : friday , saturday : saturday};

                });
                
                var table_html = "";
                table_html += "<table class='d_table display responsive'>";
                table_html += "<thead>";
                table_html += "<tr>";
                table_html += "<th>Total Flights</th>";
		table_html += "<th colspan='7' style='text-align: center'>Weekday</th>"
                table_html += "</tr>";
                table_html += "<tr>";
                table_html += "<th>Aircraft Type</th>";
		table_html += "<th>Mon</th>";
		table_html += "<th>Tue</th>";
                table_html += "<th>Wed</th>";
                table_html += "<th>Thu</th>";
                table_html += "<th>Fri</th>";
                table_html += "<th>Sat</th>";
                table_html += "<th>Sun</th>";     
                table_html += "<th></th>";
                table_html += "</tr>";
                table_html += "</thead>";
                table_html += "<tbody>";
                for(var i = 0; i < groups.length; i++)
                {
                    table_html += "<tr>";
                    table_html += "<td>"+groups[i]['aircraft_type']+"</td>";
                    table_html += "<td>"+groups[i]['monday']+"</td>";
                    table_html += "<td>"+groups[i]['tuesday']+"</td>";
                    table_html += "<td>"+groups[i]['wednesday']+"</td>";
                    table_html += "<td>"+groups[i]['thursday']+"</td>";
                    table_html += "<td>"+groups[i]['friday']+"</td>";
                    table_html += "<td>"+groups[i]['saturday']+"</td>";
                    table_html += "<td>"+groups[i]['sunday']+"</td>";
                    table_html += "<td></td>";
                    table_html += "</tr>";
                }
                table_html += "</tbody>";
                table_html += "</table>";
                return table_html;
}


function get_carrier_aircraft()
{
    $.ajax({
            type: 'GET',
            url: 'engine_flight_carrier_aircraft.php',
            dataType: 'json',
            data: {
                
            },
            success: function (data) {
                
                var table_html_arrival = "";
                var table_html_departure = "";
                if(data['arrival'].length > 0)
                {
                    $('#table_carrier_aircraft_arrival').empty();
                    table_html_arrival = make_carrier_aircraft(data['arrival'])
                    $('#table_carrier_aircraft_arrival').append(table_html_arrival);
                }
                else
                {
                    $( "#div_carrier_aircraft_arrival" ).remove();
                }
                
                if(data['departure'].length > 0)
                {
                    $('#table_carrier_aircraft_departure').empty();
                    table_html_departure = make_carrier_aircraft(data['departure'])
                    $('#table_carrier_aircraft_departure').append(table_html_departure);
                }
                else
                {
                    $( "#div_carrier_aircraft_departure" ).remove();
                }
                
                get_week_carrier();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest);
            }
        });
}

function make_carrier_aircraft(data)
{
    var array_result = []
                var group_to_values = data.reduce(function (obj, item) {
                obj[item.carrier] = obj[item.carrier] || [];
                obj[item.carrier].push({aircraft_type: item.aircraft_type,num:item.num,day_num:item.day_num});
                return obj;
                }, {});
                
                var groups = Object.keys(group_to_values).map(function (key) {
                  return {carrier_name: key , obj: group_to_values[key]};
                });
                
                for(var i = 0; i < groups.length; i++)
                {
                
                var group_to_values_aircraft = groups[i]['obj'].reduce(function (obj, item) {
                obj[item.aircraft_type] = obj[item.aircraft_type] || [];
                obj[item.aircraft_type].push({aircraft_type: item.aircraft_type,num:item.num,day_num:item.day_num});
                return obj;
                }, {});
                
                var groups_aircraft = Object.keys(group_to_values_aircraft).map(function (key) {
                    var sunday = 0;
                    var monday = 0;
                    var tuesday = 0;
                    var wednesday = 0;
                    var thursday = 0;
                    var thursday = 0
                    var friday = 0;
                    var saturday = 0;
                    for(var i =0; i < group_to_values_aircraft[key].length; i++)
                    {
                        switch(group_to_values_aircraft[key][i]['day_num']) {
                        case 0:
                          sunday = group_to_values_aircraft[key][i]['num']
                          break;
                        case 1:
                          monday = group_to_values_aircraft[key][i]['num']
                          break;
                        case 2:
                          tuesday = group_to_values_aircraft[key][i]['num']
                          break;
                        case 3:
                          wednesday = group_to_values_aircraft[key][i]['num']
                          break;
                        case 4:
                          thursday = group_to_values_aircraft[key][i]['num']
                          break;
                        case 5:
                          friday = group_to_values_aircraft[key][i]['num']
                          break;
                       case 6:
                          saturday = group_to_values_aircraft[key][i]['num']
                          break;
                        default:
                          console.log('something wrong')
                      }
                  }
                  return {aircraft_type: key , sunday: sunday , monday : monday , tuesday : tuesday , wednesday : wednesday , thursday : thursday , friday : friday , saturday : saturday};
                });
                
                array_result.push({carrier_name: groups[i]['carrier_name'],groups_aircraft: groups_aircraft})
                }
                
                var table_html = "";
                table_html += "<table class='d_table display responsive'>";
                table_html += "<thead>";
                table_html += "<tr>";
                table_html += "<th colspan='2' style='text-align: center'>Total Flights</th>";
		table_html += "<th colspan='7' style='text-align: center'>Weekday</th>"
                table_html += "</tr>";
                table_html += "<tr>";
                table_html += "<th>Carrier</th>";
                table_html += "<th>Aircraft Type</th>";
		table_html += "<th>Mon</th>";
		table_html += "<th>Tue</th>";
                table_html += "<th>Wed</th>";
                table_html += "<th>Thu</th>";
                table_html += "<th>Fri</th>";
                table_html += "<th>Sat</th>";
                table_html += "<th>Sun</th>";   
                table_html += "<th></th>";
                table_html += "</tr>";
                table_html += "</thead>";
                table_html += "<tbody>";
                for(var i = 0; i < array_result.length; i++)
                {
                   for(var m = 0; m < array_result[i]['groups_aircraft'].length;m++)
                   {
                    table_html += "<tr>";
                    if(m == 0)
                    {
                    table_html += "<td>"+array_result[i]['carrier_name']+"</td>";
                    }
                    else
                    {
                      table_html += "<td style='visibility: hidden;'>"+array_result[i]['carrier_name']+"</td>";  
                    }
                    table_html += "<td>"+array_result[i]['groups_aircraft'][m]['aircraft_type']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_aircraft'][m]['monday']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_aircraft'][m]['tuesday']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_aircraft'][m]['wednesday']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_aircraft'][m]['thursday']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_aircraft'][m]['friday']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_aircraft'][m]['saturday']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_aircraft'][m]['sunday']+"</td>";
                    table_html += "<td></td>";
                    table_html += "</tr>";
                   }
                    
                }
                table_html += "</tbody>";
                table_html += "</table>";
                
                return table_html;
}

function get_week_carrier()
{
    $.ajax({
            type: 'GET',
            url: 'engine_weekday_carrier.php',
            dataType: 'json',
            data: {
                
            },
            success: function (data) {
               
                var table_html_arrival = "";
                var table_html_departure = "";
                if(data['arrival'].length > 0)
                {
                    $('#table_weekday_carrier_arrival').empty();
                    table_html_arrival = make_weekday_carrier(data['arrival'])
                    $('#table_weekday_carrier_arrival').append(table_html_arrival);
                }
                else
                {
                    $( "#div_weekday_carrier_arrival" ).remove();
                }
                
                if(data['departure'].length > 0)
                {
                    $('#table_weekday_carrier_departure').empty();
                    table_html_departure = make_weekday_carrier(data['departure'])
                    $('#table_weekday_carrier_departure').append(table_html_departure);
                }
                else
                {
                    $( "#div_weekday_carrier_departure" ).remove();
                }
                
                $('.d_table').DataTable({
                   responsive: {
					details: {
							type: 'column',
							target: -1
					}
			},
		columnDefs: [ {
				className: 'control',
				orderable: false,
				targets:   -1
		} ]
                });
                $.unblockUI();

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest);
            }
        });
}

function make_weekday_carrier(data)
{
    var array_result = []
    var name_day = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]
    var day_week = [7,1,2,3,4,5,6]
    var group_to_values = data.reduce(function (obj, item) {
                obj[item.day_num] = obj[item.day_num] || [];
                obj[item.day_num].push({carrier_name : item.carrier,num:item.num,schedule_time: item.hour});
                return obj;
                }, {});
//    console.log(group_to_values)
     var groups = Object.keys(group_to_values).map(function (key) {
                  return {weekday: key , obj: group_to_values[key]};
                });
//    console.log(groups)

     for(var i = 0; i < groups.length; i++)
                {
                
                var group_to_values_carrier = groups[i]['obj'].reduce(function (obj, item) {
                obj[item.carrier_name] = obj[item.carrier_name] || [];
                obj[item.carrier_name].push({carrier_name: item.carrier_name,num:item.num,schedule_time:item.schedule_time});
                return obj;
                }, {});
                
                var groups_carrier = Object.keys(group_to_values_carrier).map(function (key) {
                    var hour_0 = 0;
                    var hour_1 = 0;
                    var hour_2 = 0;
                    var hour_3 = 0;
                    var hour_4 = 0;
                    var hour_5 = 0
                    var hour_6 = 0;
                    var hour_7 = 0;
                    var hour_8 = 0;
                    var hour_9 = 0;
                    var hour_10 = 0;
                    var hour_11 = 0;
                    var hour_12 = 0;
                    var hour_13 = 0;
                    var hour_14 = 0;
                    var hour_15 = 0;
                    var hour_16 = 0;
                    var hour_17 = 0;
                    var hour_18 = 0;
                    var hour_19 = 0;
                    var hour_20 = 0;
                    var hour_21 = 0;
                    var hour_22 = 0;
                    var hour_23 = 0;
                    for(var i =0; i < group_to_values_carrier[key].length; i++)
                    {
                        switch(group_to_values_carrier[key][i]['schedule_time']) {
                        case "00":
                          hour_0 = group_to_values_carrier[key][i]['num']
                          break;
                        case "01":
                          hour_1 = group_to_values_carrier[key][i]['num']
                          break;
                        case "02":
                          hour_2 = group_to_values_carrier[key][i]['num']
                          break;
                        case "03":
                          hour_3 = group_to_values_carrier[key][i]['num']
                          break;
                        case "04":
                          hour_4 = group_to_values_carrier[key][i]['num']
                          break;
                        case "05":
                          hour_5 = group_to_values_carrier[key][i]['num']
                          break;
                       case "06":
                          hour_6 = group_to_values_carrier[key][i]['num']
                          break;
                      case "07":
                          hour_7 = group_to_values_carrier[key][i]['num']
                          break;
                       case "08":
                          hour_8 = group_to_values_carrier[key][i]['num']
                          break;
                       case "09":
                          hour_9 = group_to_values_carrier[key][i]['num']
                          break;
                       case "10":
                          hour_10= group_to_values_carrier[key][i]['num']
                          break;
                       case "11":
                          hour_11 = group_to_values_carrier[key][i]['num']
                          break;
                       case "12":
                          hour_12 = group_to_values_carrier[key][i]['num']
                          break;
                        case "13":
                          hour_13 = group_to_values_carrier[key][i]['num']
                          break;
                        case "14":
                          hour_14 = group_to_values_carrier[key][i]['num']
                          break;
                        case "15":
                          hour_15 = group_to_values_carrier[key][i]['num']
                          break;
                         case "16":
                          hour_16 = group_to_values_carrier[key][i]['num']
                          break;
                          case "17":
                          hour_17 = group_to_values_carrier[key][i]['num']
                          break;
                          case "18":
                          hour_18 = group_to_values_carrier[key][i]['num']
                          break;
                          case "19":
                          hour_19 = group_to_values_carrier[key][i]['num']
                          break;
                          case "20":
                          hour_20 = group_to_values_carrier[key][i]['num']
                          break;
                          case "21":
                          hour_21 = group_to_values_carrier[key][i]['num']
                          break;
                          case "22":
                          hour_22 = group_to_values_carrier[key][i]['num']
                          break;
                          case "23":
                          hour_23 = group_to_values_carrier[key][i]['num']
                          break;
                        default:
                          console.log('something wrong')
                      }
                  }
                  return {carrier_name: key , hour_0: hour_0 , hour_1 : hour_1 , hour_2 : hour_2 , hour_3 : hour_3 , hour_4 : hour_4 
                      , hour_5 : hour_5 , hour_6 : hour_6, hour_7 : hour_7, hour_8 : hour_8, hour_9 : hour_9, hour_10 : hour_10, hour_11 : hour_11
                  , hour_12 : hour_12 , hour_13 : hour_13 , hour_14 : hour_14 , hour_15 : hour_15 , hour_16 : hour_16, hour_17 : hour_17
              , hour_18 : hour_18, hour_19 : hour_19, hour_20 : hour_20, hour_21 : hour_21, hour_22 : hour_22, hour_23 : hour_23};
                });
//                
                array_result.push({weekday: name_day[groups[i]['weekday']],sort_day: day_week[groups[i]['weekday']],groups_carrier: groups_carrier})
                }
                
                console.log(array_result)
                
                var table_html = "";
                table_html += "<table class='d_table display responsive'>";
                table_html += "<thead>";
                table_html += "<tr>";
                table_html += "<th colspan='3' style='text-align: center'>Total Flights</th>";
		table_html += "<th colspan='24' style='text-align: center'>Hours</th>"
                table_html += "</tr>";
                table_html += "<tr>";
                table_html += "<th></th>";
                table_html += "<th>Weekday</th>";
                table_html += "<th>Carrier</th>";
                table_html += "<th>00</th>";
		table_html += "<th>01</th>";
		table_html += "<th>02</th>";
                table_html += "<th>03</th>";
                table_html += "<th>04</th>";
                table_html += "<th>05</th>";
                table_html += "<th>06</th>";
                table_html += "<th>07</th>";
                table_html += "<th>08</th>";
		table_html += "<th>09</th>";
		table_html += "<th>10</th>";
                table_html += "<th>11</th>";
                table_html += "<th>12</th>";
                table_html += "<th>13</th>";
                table_html += "<th>14</th>";
                table_html += "<th>15</th>";
                table_html += "<th>16</th>";
		table_html += "<th>17</th>";
		table_html += "<th>18</th>";
                table_html += "<th>19</th>";
                table_html += "<th>20</th>";
                table_html += "<th>21</th>";
                table_html += "<th>22</th>";
                table_html += "<th>23</th>";
                table_html += "<th></th>";
                table_html += "</tr>";
                table_html += "</thead>";
                table_html += "<tbody>";
                for(var i = 0; i < array_result.length; i++)
                {
                   for(var m = 0; m < array_result[i]['groups_carrier'].length;m++)
                   {
                    table_html += "<tr>";
                    table_html += "<td style='visibility: hidden;'>"+array_result[i]['sort_day']+"</td>";  
                    if(m == 0)
                    {
                    table_html += "<td>"+array_result[i]['weekday']+"</td>";
                    }
                    else
                    {
                      table_html += "<td style='visibility: hidden;'>"+array_result[i]['weekday']+"</td>";  
                    }
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['carrier_name']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_0']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_1']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_2']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_3']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_4']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_5']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_6']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_7']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_8']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_9']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_10']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_11']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_12']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_13']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_14']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_15']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_16']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_17']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_18']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_19']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_20']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_21']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_22']+"</td>";
                    table_html += "<td>"+array_result[i]['groups_carrier'][m]['hour_23']+"</td>";
                    table_html += "<td></td>";
                    table_html += "</tr>";
                   }
                    
                }
                table_html += "</tbody>";
                table_html += "</table>";
                
                return table_html;
}