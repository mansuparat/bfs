$(document).ready(function () {
//    console.log('ready')
    $( ".datepicker" ).datepicker({
       dateFormat: 'dd/mm/yy'
    }).datepicker("setDate", new Date());
    //datepicker
    $('.d_table').DataTable({
    responsive: true
});
    //datatable
    $('input.numberinput').bind('keypress', function (e) {
        return (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 46) ? false : true;
    });
    //number only
});

function BlockUI()
    {
        $.blockUI({ message: '<h1> Just a moment...</h1>' }); 
    }
    
function search_flight()
{
    BlockUI()
    var flight_type = $('.flight_type:checked').val();
    var time_type = $('.time_type:checked').val();
    var hour_from = $('#hour_from').val();
    var hour_to = $('#hour_to').val();
    var datepicker_from = $('#datepicker_from').val();
    var datepicker_to = $('#datepicker_to').val();
    var carrier = $('#carrier').val();
    var flt_no = $('#flt_no').val();
    var reg = $('#reg').val();
    var gate = $('#gate').val();
    var pos = $('#pos').val();
    datepicker_from = reverse_date(datepicker_from)
    datepicker_to = reverse_date(datepicker_to)
    datepicker_from = moment(datepicker_from).format('YYYY-MM-DD 00:00')
    datepicker_to = moment(datepicker_to).format('YYYY-MM-DD 23:59')
    
    hour_from = moment().subtract(hour_from, 'hours').format('YYYY-MM-DD HH:mm')
    hour_to = moment().add(hour_to, 'hours').format('YYYY-MM-DD HH:mm')
    $.ajax({
            type: 'GET',
            url: 'engine_search_dashboard.php',
            dataType: 'json',
            data: {
                flight_type: flight_type,
                time_type : time_type,
                hour_from : hour_from,
                hour_to : hour_to,
                datepicker_from : datepicker_from,
                datepicker_to : datepicker_to,
                carrier : carrier,
                flt_no : flt_no,
                reg : reg,
                gate : gate,
                pos : pos,
                
            },
            success: function (data) {
                var table_html = "";
                table_html += "<table class='d_table display responsive'>";
                table_html += "<thead>";
                table_html += "<tr>";
                table_html += "<td>Schedule Date</td>";
                table_html += "<td>Carrier</td>";
                table_html += "<td>Flight No</td>";
                table_html += "<td>Aircraft Type</td>";
                table_html += "<td>Gate</td>";                
                table_html += "</tr>";
                table_html += "</thead>";
                table_html += "<tbody>";
                for(var i = 0; i < data.length; i++)
                {
                    var myJSON = JSON.stringify(data[i]);
                    table_html += "<tr onclick='open_modal_detail("+myJSON+")'>";
                    table_html += "<td>"+data[i]['schedule_date']+"</td>";
                    table_html += "<td>"+data[i]['carrier']+"</td>";
                    table_html += "<td>"+data[i]['flight_no']+"</td>";
                    table_html += "<td>"+data[i]['aircraft_type']+"</td>";
                    if(data[i]['gate'] == null)
                    {
                      table_html += "<td>-</td>";  
                    } else {
                    table_html += "<td>"+data[i]['gate']+"</td>";
                    }
                    table_html += "</tr>";
                }
                table_html += "</tbody>";
                table_html += "</table>";
                $('#table_result').empty();
		$('#table_result').append(table_html);
                $('.d_table').DataTable({
    responsive: true
});
$.unblockUI();
//                console.log(data)
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest);
            }
        });
        
    
}

function open_modal_detail(data)
{
    $("#modal_header").text('Flight number : '+ data['flight_no'])
    $("#regis").text('Registration : '+ data['reg'])
    $("#belt").text('Belt : '+ data['belt'])
    $("#remark").text('Remark : '+ data['remark'])
    $("#myModal").modal('show');
}

function reverse_date(date)
{
    var d=new Date(date.split("/").reverse().join("-"));
var dd=d.getDate();
var mm=d.getMonth()+1;
var yy=d.getFullYear();
var newdate=yy+"/"+mm+"/"+dd;
    return newdate;
}

function change_time_type()
{
    var time_type = $('.time_type:checked').val();
    if(time_type == 'hour')
    {
        $('#hour_from').prop( "disabled", false );
        $('#hour_to').prop( "disabled", false );
        $('#datepicker_from').prop( "disabled", true );
        $('#datepicker_to').prop( "disabled", true );
    }
    else
    {
        $('#hour_from').prop( "disabled", true );
        $('#hour_to').prop( "disabled", true );
        $('#datepicker_from').prop( "disabled", false );
        $('#datepicker_to').prop( "disabled", false );
    } //date
}