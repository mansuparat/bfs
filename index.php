<?php
include("database/connect.php");
?>
<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
            <?php include_once("style_file.php");?>
	</head>
	<body class="is-preload">

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<div id="main">
						<div class="inner">

							<!-- Header -->
								<header id="header">
									<a href="index.php" class="logo"><strong>Editorial</strong> by HTML5 UP</a>
								</header>


							<!-- Section -->
								<section>
									<div class="features">
                                                                            <div class="row gtr-uniform">
                                                                           <div class="col-4 col-12-small">
                                                                               <h4> Arr/Dep :</h4>
															</div>
                                                                                
                                                                           <div class="col-4 col-12-small">
																<input type="radio" id="flight_type_arrival" class="flight_type" name="flight_type" value="Arrival" checked>
																<label for="flight_type_arrival">Arrival</label>
															</div>
															<div class="col-4 col-12-small">
																<input type="radio" id="flight_type_departure" class="flight_type" name="flight_type" value="Departure">
																<label for="flight_type_departure">Departure</label>
															</div>
                                                                
														</div>
									</div>
                                                                    <div class="features">
                                                                        <div class="row gtr-uniform">
                                                                           <div class="col-2 col-12-small">
						
                                                                                                                                <h4> Range : </h4>
															</div>
                                                                                
                                                                           <div class="col-2 col-12-small">
																<input onchange="change_time_type()" type="radio" id="time_type_hour" class="time_type" name="time_type" value="hour" checked>
                                                                                                                                <label for="time_type_hour"></label>
                                                                                                                                
															</div>
                                                                            <div class="col-1 col-12-small">
						
                                                                                                                                <h4> - </h4>
															</div>
                                                                            <div class="col-2 col-12-xsmall">
																<input type="text" name="hour_from" id="hour_from" class="numberinput" value="1">
															</div>
                                                                            <div class="col-2 col-12-xsmall">
															Hour to
															</div>
                                                                            <div class="col-1 col-12-small">
						
                                                                                                                                <h4> + </h4>
															</div>
                                                                            <div class="col-2 col-12-xsmall">
																<input type="text" name="hour_to" id="hour_to" class="numberinput" value="1">
															</div>
                                                                            
															
                                                                
														</div>
                                                                    </div>
                                                                    
                                                                    <div class="features">
                                                                        <div class="row gtr-uniform">
                                                                            <div class="col-2 col-12-small">
						
                                                                                                                                <h4></h4>
															</div>
                                                                            
															<div class="col-3 col-12-small">
																<input onchange="change_time_type()" type="radio" id="time_type_date" class="time_type" name="time_type" value="date">
																<label for="time_type_date">Schedule Date Range : </label>
															</div>
                                                                            <div class="col-3 col-12-xsmall">
																 <input type="text" class="datepicker" data-date-format='yy-mm-dd' id="datepicker_from" disabled>
															</div>
                                                                
														
                                                                        <div class="col-3 col-12-xsmall">
																 <input type="text" class="datepicker" data-date-format='yy-mm-dd' id="datepicker_to" disabled>
															</div>
                                                                
												</div>	
                                                                    </div>
                                                                    
                                                                    <div class="features">
                                                                        <div class="row gtr-uniform">
                                                                            <div class="col-1 col-12-small">
						
                                                                                                                                <h4>Carrier : </h4>
															</div>
                                                                            
															<div class="col-2 col-12-small">
																<input type="text" name="carrier" id="carrier" value="">
															</div>
                                                                            <div class="col-1 col-12-xsmall">
															<h4>Flt No. : </h4>
															</div>
                                                                            <div class="col-2 col-12-small">
																<input type="text" name="flt_no" id="flt_no" value="">
															</div>
                                                                            <div class="col-1 col-12-xsmall">
															<h4>REG : </h4>
															</div>
                                                                            <div class="col-2 col-12-small">
																<input type="text" name="reg" id="reg" value="">
															</div>
                                                                            <div class="col-1 col-12-xsmall">
															<h4>GATE : </h4>
															</div>
                                                                            <div class="col-2 col-12-small">
																<input type="text" name="gate" id="gate" value="">
															</div>
                                                                            <div class="col-1 col-12-xsmall">
															<h4>POS : </h4>
															</div>
                                                                            <div class="col-2 col-12-small">
																<input type="text" name="pos" id="pos" value="">
															</div>
                                                                            <div class="col-2 col-12-small">
																<ul class="actions">
                                                                                                                                    <li><input type="button" onclick="search_flight()" value="Submit" class="primary"></li>
					
																</ul>
															</div>
                                              
                                                                
												</div>	
                                                                    </div>
                                                                    
                                                                    <div class="features">
                                                                        <div class="row gtr-uniform" style="width:100%">
                                                                            <div class="col-12" id="table_result">
                                                                        
                                              <div class="">        
														<table class="d_table display responsive">
															<thead>
																<tr>
																	<th>Schedule Date</th>
																	<th>Carrier</th>
																	<th>Flight No</th>
                                                                                                                                        <th>Aircraft Type</th>
                                                                                                                                        <th>Gate</th>
																</tr>
															</thead>
															<tbody>
																
															</tbody>
														</table>
													</div>
                                                                            </div>
                                                                            
                                                                
												</div>	
                                                                    </div>
								</section>


						</div>
					</div>

				<!-- Sidebar -->
					<div id="sidebar">
						<div class="inner">

							<!-- Menu -->
								<nav id="menu">
									<header class="major">
										<h2>Menu</h2>
									</header>
									<ul>
										<?php include_once("list_menu.php");?>
									</ul>
								</nav>


							<!-- Section -->
								<section>
									<header class="major">
										<h2>Get in touch</h2>
									</header>
									<ul class="contact">
										<li class="icon solid fa-envelope"><a href="#">mansuparatkit@gmail.com</a></li>
										<li class="icon solid fa-phone">(+66) 83-039-8613</li>
									</ul>
								</section>

							<!-- Footer -->
								<footer id="footer">
									<p class="copyright">&copy; Untitled. All rights reserved. Demo Images: <a href="https://unsplash.com">Unsplash</a>. Design: <a href="https://html5up.net">HTML5 UP</a>.</p>
								</footer>

						</div>
					</div>
<?php include_once("script_file.php");?>
                                
                                <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog" >
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="modal_header"></h4>
        </div>
        <div class="modal-body">
          <p id="regis"></p>
          <p id="belt"></p>
          <p id="remark"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
			</div>

	</body>
</html>