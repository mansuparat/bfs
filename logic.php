<?php
include("database/connect.php");
?>
<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
            <?php include_once("style_file.php");?>
	</head>
        <style>
            .wrapper {
    box-sizing: border-box;
    padding: 5px;
    width: 100%;
}

.wrapper .row2 {
    font-size: 0px;
    text-align: center;
}

.wrapper .row2 * {
    font-size: 14px;
}

.wrapper .block {
    color: black;
    text-align: center;
    
    display: inline-block;
    margin: 1%;
    margin-bottom: 5px;
    width: 2%;
}
        </style>
	<body class="is-preload">

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<div id="main">
						<div class="inner">

							<!-- Header -->
								<header id="header">
									<a href="index.php" class="logo"><strong>Editorial</strong> by HTML5 UP</a>
								</header>


							<!-- Section -->
								<section>
                                                                    <div id="show_pic_dimesion1" style="text-align: center">
                                                                        
                                                                    </div>
                                                                    <br>
                                                                    <div id="result_dimesion1"></div>
                                                                    
                                                                    <div id="show_pic_dimesion2" style="text-align: center">
                                                                        
                                                                    </div>
                                                                    <br>
                                                                    <div id="result_dimesion2"></div>
                                                                    
								</section>


						</div>
					</div>

				<!-- Sidebar -->
					<div id="sidebar">
						<div class="inner">

							<!-- Menu -->
								<nav id="menu">
									<header class="major">
										<h2>Menu</h2>
									</header>
									<ul>
										<?php include_once("list_menu.php");?>
									</ul>
								</nav>


							<!-- Section -->
								<section>
									<header class="major">
										<h2>Get in touch</h2>
									</header>
									<ul class="contact">
										<li class="icon solid fa-envelope"><a href="#">mansuparatkit@gmail.com</a></li>
										<li class="icon solid fa-phone">(+66) 83-039-8613</li>
									</ul>
								</section>

							<!-- Footer -->
								<footer id="footer">
									<p class="copyright">&copy; Untitled. All rights reserved. Demo Images: <a href="https://unsplash.com">Unsplash</a>. Design: <a href="https://html5up.net">HTML5 UP</a>.</p>
								</footer>

						</div>
					</div>
<?php include_once("script_file.php");?>
<script src="js/logic.js"></script>     
			</div>

	</body>
</html>