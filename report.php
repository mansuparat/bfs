<?php
include("database/connect.php");
?>
<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
            <?php include_once("style_file.php");?>
	</head>
	<body class="is-preload">

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<div id="main">
						<div class="inner">

							<!-- Header -->
								<header id="header">
									<a href="index.php" class="logo"><strong>Editorial</strong> by HTML5 UP</a>
								</header>


							<!-- Section -->
								<section>
                                                                    <div id="div_carrier_arrival">
									<div class="features">
                                                                            <div class="row gtr-uniform">
                                                                           <div class="col-12 col-12-small">
                                                                              
                                                                               <h4>สรุปจำนวนเที่ยวบินแสดงตาม Carrier (ขาเข้า)</h4>
                                                                      
									</div>
                                                                           </div>
									</div>
                                                                        <div class="features">
                                                                        <div class="row gtr-uniform" style="width:100%">
                                                                            <div class="col-12" id="table_carrier_arrival">
                                                                        
                                                                                <div class="">        
														<table class="d_table display responsive">
															<thead>
                                                                                                                                <tr>
																	<th>Total Flights</th>
																	<th colspan="7" style="text-align: center">Weekday</th>
																</tr>
																<tr>
																	<th>Carrier</th>
																	<th>Mon</th>
																	<th>Tue</th>
                                                                                                                                        <th>Wed</th>
                                                                                                                                        <th>Thu</th>
                                                                                                                                        <th>Fri</th>
                                                                                                                                        <th>Sat</th>
                                                                                                                                        <th>Sun</th>
                                                                                                                                        <th></th>
																</tr>
															</thead>
															<tbody>
                                                                                                                                    
															</tbody>
														</table>
													</div>
                                                                            </div>
                                                                            
                                                                
												</div>	
                                                                    </div>
                                                                    </div>
                                                                    <div id="div_carrier_departure">
                                                                        <div class="features">
                                                                            <div class="row gtr-uniform">
                                                                           <div class="col-12 col-12-small">
                                                                              
                                                                               <h4>สรุปจำนวนเที่ยวบินแสดงตาม Carrier (ขาออก)</h4>
                                                                      
									</div>
                                                                           </div>
									</div>
                                                                        <div class="features">
                                                                        <div class="row gtr-uniform" style="width:100%">
                                                                            <div class="col-12" id="table_carrier_departure">
                                                                        
                                                                                <div class="">        
														<table class="d_table display responsive">
															<thead>
                                                                                                                                <tr>
																	<th>Total Flights</th>
																	<th colspan="7" style="text-align: center">Weekday</th>
																</tr>
																<tr>
																	<th>Carrier</th>
																	<th>Mon</th>
																	<th>Tue</th>
                                                                                                                                        <th>Wed</th>
                                                                                                                                        <th>Thu</th>
                                                                                                                                        <th>Fri</th>
                                                                                                                                        <th>Sat</th>
                                                                                                                                        <th>Sun</th>
                                                                                                                                        <th></th>
																</tr>
															</thead>
															<tbody>
																
															</tbody>
														</table>
													</div>
                                                                            </div>
                                                                            
                                                                
												</div>	
                                                                    </div>
                                                                    </div>
                                                                    
                                                                    <div id="div_aircraft_arrival">
                                                                        <div class="features">
                                                                            <div class="row gtr-uniform">
                                                                           <div class="col-12 col-12-small">
                                                                              
                                                                               <h4>สรุปจำนวนเที่ยวบินแสดงตาม Aircraft Type (ขาเข้า)</h4>
                                                                      
									</div>
                                                                           </div>
									</div>
                                                                        <div class="features">
                                                                        <div class="row gtr-uniform" style="width:100%">
                                                                            <div class="col-12" id="table_aircraft_arrival">
                                                                        
                                                                                <div class="">        
														<table class="d_table display responsive">
															<thead>
                                                                                                                                <tr>
																	<th>Total Flights</th>
																	<th colspan="7" style="text-align: center">Weekday</th>
																</tr>
																<tr>
																	<th>Aircraft Type</th>
																	<th>Mon</th>
																	<th>Tue</th>
                                                                                                                                        <th>Wed</th>
                                                                                                                                        <th>Thu</th>
                                                                                                                                        <th>Fri</th>
                                                                                                                                        <th>Sat</th>
                                                                                                                                        <th>Sun</th>
                                                                                                                                        <th></th>
																</tr>
															</thead>
															<tbody>
																
															</tbody>
														</table>
													</div>
                                                                            </div>
                                                                            
                                                                
												</div>	
                                                                    </div>
                                                                    </div>
                                                                    
                                                                    <div id="div_aircraft_departure">
                                                                        <div class="features">
                                                                            <div class="row gtr-uniform">
                                                                           <div class="col-12 col-12-small">
                                                                              
                                                                               <h4>สรุปจำนวนเที่ยวบินแสดงตาม Aircraft Type (ขาออก)</h4>
                                                                      
									</div>
                                                                           </div>
									</div>
                                                                        <div class="features">
                                                                        <div class="row gtr-uniform" style="width:100%">
                                                                            <div class="col-12" id="table_aircraft_departure">
                                                                        
                                                                                <div class="">        
														<table class="d_table display responsive">
															<thead>
                                                                                                                                <tr>
																	<th>Total Flights</th>
																	<th colspan="7" style="text-align: center">Weekday</th>
																</tr>
																<tr>
																	<th>Aircraft Type</th>
																	<th>Mon</th>
																	<th>Tue</th>
                                                                                                                                        <th>Wed</th>
                                                                                                                                        <th>Thu</th>
                                                                                                                                        <th>Fri</th>
                                                                                                                                        <th>Sat</th>
                                                                                                                                        <th>Sun</th>
                                                                                                                                        <th></th>
																</tr>
															</thead>
															<tbody>
																
															</tbody>
														</table>
													</div>
                                                                            </div>
                                                                            
                                                                
												</div>	
                                                                    </div>
                                                                    </div>
                                                                    
                                                                    <div id="div_carrier_aircraft_arrival">
                                                                        <div class="features">
                                                                            <div class="row gtr-uniform">
                                                                           <div class="col-12 col-12-small">
                                                                              
                                                                               <h4>สรุปจำนวนเที่ยวบินแสดงตาม Carrier และ Aircraft Type (ขาเข้า)</h4>
                                                                      
									</div>
                                                                           </div>
									</div>
                                                                        <div class="features">
                                                                        <div class="row gtr-uniform" style="width:100%">
                                                                            <div class="col-12" id="table_carrier_aircraft_arrival">
                                                                        
                                                                                <div class="">        
														<table class="d_table display responsive">
															<thead>
                                                                                                                                <tr>
                                                                                                                                        
																	<th colspan="2" style="text-align: center">Total Flights</th>
																	<th colspan="7" style="text-align: center">Weekday</th>
                                                                                                                                        <th></th>
																</tr>
																<tr>
                                                                                                                                        
                                                                                                                                        <th>Carrier</th>
																	<th>Aircraft Type</th>
																	<th>Mon</th>
																	<th>Tue</th>
                                                                                                                                        <th>Wed</th>
                                                                                                                                        <th>Thu</th>
                                                                                                                                        <th>Fri</th>
                                                                                                                                        <th>Sat</th>
                                                                                                                                        <th>Sun</th>
                                                                                                                                        <th></th>
																</tr>
															</thead>
															<tbody>
																
															</tbody>
														</table>
													</div>
                                                                            </div>
                                                                            
                                                                
												</div>	
                                                                    </div>
                                                                    </div>
                                                                    
                                                                    <div id="div_carrier_aircraft_departure">
                                                                        <div class="features">
                                                                            <div class="row gtr-uniform">
                                                                           <div class="col-12 col-12-small">
                                                                              
                                                                               <h4>สรุปจำนวนเที่ยวบินแสดงตาม Carrier และ Aircraft Type (ขาออก)</h4>
                                                                      
									</div>
                                                                           </div>
									</div>
                                                                        <div class="features">
                                                                        <div class="row gtr-uniform" style="width:100%">
                                                                            <div class="col-12" id="table_carrier_aircraft_departure">
                                                                        
                                                                                <div class="">        
														<table class="d_table display responsive">
															<thead>
                                                                                                                                <tr>
                                                                                                                                        
																	<th colspan="2" style="text-align: center">Total Flights</th>
																	<th colspan="7" style="text-align: center">Weekday</th>
                                                                                                                                        <th></th>
																</tr>
																<tr>
                                                                                                                                        
                                                                                                                                        <th>Carrier</th>
																	<th>Aircraft Type</th>
																	<th>Mon</th>
																	<th>Tue</th>
                                                                                                                                        <th>Wed</th>
                                                                                                                                        <th>Thu</th>
                                                                                                                                        <th>Fri</th>
                                                                                                                                        <th>Sat</th>
                                                                                                                                        <th>Sun</th>
                                                                                                                                        <th></th>
																</tr>
															</thead>
															<tbody>
																
															</tbody>
														</table>
													</div>
                                                                            </div>
                                                                            
                                                                
												</div>	
                                                                    </div>
                                                                    </div>
                                                                    
                                                                    
                                                                     <div id="div_weekday_carrier_arrival">
                                                                        <div class="features">
                                                                            <div class="row gtr-uniform">
                                                                           <div class="col-12 col-12-small">
                                                                              
                                                                               <h4>สรุปจำนวนเที่ยวบินแสดงตาม Weekday, Carrier (ขาเข้า)</h4>
                                                                      
									</div>
                                                                           </div>
									</div>
                                                                        <div class="features">
                                                                        <div class="row gtr-uniform" style="width:100%">
                                                                            <div class="col-12" id="table_weekday_carrier_arrival">
                                                                        
                                                                                <div class="">        
														<table class="d_table display responsive">
															<thead>
                                                                                                                                <tr>
                                                                                                                                        
																	<th colspan="2" style="text-align: center">Total Flights</th>
																	<th colspan="24" style="text-align: center">Hours</th>
                                                                                                                                        <th></th>
																</tr>
																<tr>
                                                                                                                                        <th>Weekday</th>
                                                                                                                                        <th>Carrier</th>
																	<th>00</th>
																	<th>01</th>
                                                                                                                                        <th>02</th>
                                                                                                                                        <th>03</th>
                                                                                                                                        <th>04</th>
                                                                                                                                        <th>05</th>
                                                                                                                                        <th>06</th>
                                                                                                                                        <th>07</th>
                                                                                                                                        <th>08</th>
                                                                                                                                        <th>09</th>
                                                                                                                                        <th>10</th>
                                                                                                                                        <th>11</th>
                                                                                                                                        <th>12</th>
                                                                                                                                        <th>13</th>
                                                                                                                                        <th>14</th>
                                                                                                                                        <th>15</th>
                                                                                                                                        <th>16</th>
                                                                                                                                        <th>17</th>
                                                                                                                                        <th>18</th>
                                                                                                                                        <th>19</th>
                                                                                                                                        <th>20</th>
                                                                                                                                        <th>21</th>
                                                                                                                                        <th>22</th>
                                                                                                                                        <th>23</th>
                                                                                                                                        <th></th>
																</tr>
															</thead>
															<tbody>
																
															</tbody>
														</table>
													</div>
                                                                            </div>
                                                                            
                                                                
												</div>	
                                                                    </div>
                                                                    </div>
                                                                    
                                                                    <div id="div_weekday_carrier_departure">
                                                                        <div class="features">
                                                                            <div class="row gtr-uniform">
                                                                           <div class="col-12 col-12-small">
                                                                              
                                                                               <h4>สรุปจำนวนเที่ยวบินแสดงตาม Weekday, Carrier (ขาออก)</h4>
                                                                      
									</div>
                                                                           </div>
									</div>
                                                                        <div class="features">
                                                                        <div class="row gtr-uniform" style="width:100%">
                                                                            <div class="col-12" id="table_weekday_carrier_departure">
                                                                        
                                                                                <div class="">        
														<table class="d_table display responsive">
															<thead>
                                                                                                                                <tr>
                                                                                                                                        
																	<th colspan="2" style="text-align: center">Total Flights</th>
																	<th colspan="24" style="text-align: center">Hours</th>
                                                                                                                                        <th></th>
																</tr>
																<tr>
                                                                                                                                        <th>Weekday</th>
                                                                                                                                        <th>Carrier</th>
																	<th>00</th>
																	<th>01</th>
                                                                                                                                        <th>02</th>
                                                                                                                                        <th>03</th>
                                                                                                                                        <th>04</th>
                                                                                                                                        <th>05</th>
                                                                                                                                        <th>06</th>
                                                                                                                                        <th>07</th>
                                                                                                                                        <th>08</th>
                                                                                                                                        <th>09</th>
                                                                                                                                        <th>10</th>
                                                                                                                                        <th>11</th>
                                                                                                                                        <th>12</th>
                                                                                                                                        <th>13</th>
                                                                                                                                        <th>14</th>
                                                                                                                                        <th>15</th>
                                                                                                                                        <th>16</th>
                                                                                                                                        <th>17</th>
                                                                                                                                        <th>18</th>
                                                                                                                                        <th>19</th>
                                                                                                                                        <th>20</th>
                                                                                                                                        <th>21</th>
                                                                                                                                        <th>22</th>
                                                                                                                                        <th>23</th>
                                                                                                                                        <th></th>
																</tr>
															</thead>
															<tbody>
																
															</tbody>
														</table>
													</div>
                                                                            </div>
                                                                            
                                                                
												</div>	
                                                                    </div>
                                                                    </div>
                    
								</section>


						</div>
					</div>

				<!-- Sidebar -->
					<div id="sidebar">
						<div class="inner">

							<!-- Menu -->
								<nav id="menu">
									<header class="major">
										<h2>Menu</h2>
									</header>
									<ul>
										<?php include_once("list_menu.php");?>
									</ul>
								</nav>


							<!-- Section -->
								<section>
									<header class="major">
										<h2>Get in touch</h2>
									</header>
									<ul class="contact">
										<li class="icon solid fa-envelope"><a href="#">mansuparatkit@gmail.com</a></li>
										<li class="icon solid fa-phone">(+66) 83-039-8613</li>
									</ul>
								</section>

							<!-- Footer -->
								<footer id="footer">
									<p class="copyright">&copy; Untitled. All rights reserved. Demo Images: <a href="https://unsplash.com">Unsplash</a>. Design: <a href="https://html5up.net">HTML5 UP</a>.</p>
								</footer>

						</div>
					</div>
<?php include_once("script_file.php");?>
<script src="js/report.js"></script>                                
                                <!-- Modal -->
			</div>

	</body>
</html>